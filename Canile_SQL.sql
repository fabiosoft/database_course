DROP TABLE Foto;
DROP TABLE Visita;
DROP TABLE Uscita;
DROP TABLE Entrata;
DROP TABLE Cane;
DROP TABLE Cuccia;
DROP TABLE Razze;
DROP TABLE Padrone;
DROP TABLE Lavora_su;
DROP TABLE Missione;
DROP TABLE Usa_Veicolo;
DROP TABLE Operatore;
DROP TABLE Distretto;
DROP TABLE Comune;
DROP TABLE ASL;
DROP TABLE Armadietto;
DROP TABLE Veicolo;

CREATE TABLE Cuccia
(
Cod_Cuccia CHAR(20),
Dimensione_mq2 NUMBER(10),
PRIMARY KEY (Cod_Cuccia)
);

CREATE TABLE Razze
(
Cod_Razza CHAR(20),
Nome CHAR(20),
PRIMARY KEY (Cod_Razza)
);

CREATE TABLE Padrone
(
CodFisc CHAR(16),
Cognome CHAR(20) NOT NULL,
Nome CHAR(20) NOT NULL,
Citta CHAR(20),
Tel CHAR(10),
Cap NUMBER(6),
CIVICO NUMBER(6),
Via CHAR(50),
PRIMARY KEY (CodFisc)
);

CREATE TABLE Cane
(
Microchip CHAR(20),
AnnoN DATE,
Sesso CHAR(1),
Padrone CHAR(16),
Cuccia CHAR(20),
Razza CHAR(20),
PRIMARY KEY (Microchip)
);

CREATE TABLE Foto
(
URL CHAR(20),
data DATE,
Microchip CHAR(20),
PRIMARY KEY (URL)
);

CREATE TABLE Visita
(
Microchip CHAR(20),
Data DATE,
Diagnosi CHAR(20),
Terapia CHAR(20),
PRIMARY KEY (Microchip)
);

CREATE TABLE Distretto
(
Numero NUMBER(5),
Nome CHAR(20),
Civico NUMBER(4),
Cap NUMBER(6),
Via CHAR(50),
Citta CHAR(50),
PRIMARY KEY (Numero)
);

CREATE TABLE ASL
(
n_asl CHAR(20),
Responsabile CHAR(20),
Cap NUMBER(6),
Civico NUMBER(4),
Via CHAR(50),
Citta CHAR(50),
PRIMARY KEY (n_asl)
);

CREATE TABLE Comune
(
CodComune CHAR(20),
Nome CHAR(20),
Sindaco CHAR(20),
ASL CHAR(20),
PRIMARY KEY (CodComune)
);

CREATE TABLE Missione
(
Nome_Missione CHAR(20) NOT NULL,
N_Missione NUMBER(3),
Loc_Missione CHAR(50),
Distretto NUMBER(5) NOT NULL,
PRIMARY KEY (N_Missione)
);

CREATE TABLE Armadietto
(
Numero NUMBER(5),
Combinazione NUMBER(10),
Domensione CHAR(20),
PRIMARY KEY (Numero)
);

CREATE TABLE Operatore
(
CF CHAR(16),
Cognome CHAR(20) NOT NULL,
Nome CHAR(20) NOT NULL,
Indirizzo CHAR(50),
Citta CHAR(20),
Tel CHAR(10),
Cell CHAR(20),
Salario NUMBER(10) NOT NULL,
Distretto NUMBER(5) NOT NULL,
Armadietto NUMBER(5),
PRIMARY KEY (CF)
);

CREATE TABLE Entrata
(
n_progressivo CHAR(20),
DataE DATE,
ModoE CHAR(20),
Comune CHAR(20),
Microchip CHAR(20),
Operatore CHAR(16),
PRIMARY KEY (n_progressivo)
);

CREATE TABLE Uscita
(
n_reg_uscita CHAR(20),
n_reg_entrata CHAR(20),
DataU DATE,
ModoU CHAR(20),
Note CHAR(20),
PRIMARY KEY (n_reg_uscita)
);

CREATE TABLE Veicolo
(
Targa CHAR(10),
Posti NUMBER(2),
PRIMARY KEY (Targa)
);

CREATE TABLE Usa_Veicolo
(
Targa_veicolo CHAR(10),
Operatore CHAR(16),
PRIMARY KEY (Targa_veicolo,Operatore)
);

CREATE TABLE Lavora_su
(
Operatore CHAR(16) NOT NULL,
Missione NUMBER(3) NOT NULL,
Ore NUMBER(4),
PRIMARY KEY (Operatore,Missione)
);

ALTER TABLE Cane ADD FOREIGN KEY (Padrone) REFERENCES Padrone (CodFisc);

ALTER TABLE Cane ADD FOREIGN KEY (Cuccia) REFERENCES Cuccia (Cod_Cuccia);

ALTER TABLE Cane ADD FOREIGN KEY (Razza) REFERENCES Razze (Cod_Razza);

ALTER TABLE Foto ADD FOREIGN KEY (Microchip) REFERENCES Cane (Microchip);

ALTER TABLE Visita ADD FOREIGN KEY (Microchip) REFERENCES Cane (Microchip);

ALTER TABLE Comune ADD FOREIGN KEY (ASL) REFERENCES ASL (n_asl);

ALTER TABLE Missione ADD FOREIGN KEY (Distretto) REFERENCES Distretto (Numero);

ALTER TABLE Operatore ADD FOREIGN KEY (Distretto) REFERENCES Distretto (Numero);

ALTER TABLE Operatore ADD FOREIGN KEY (Armadietto) REFERENCES Armadietto (Numero);

ALTER TABLE Entrata ADD FOREIGN KEY (Comune) REFERENCES Comune (CodComune);

ALTER TABLE Entrata ADD FOREIGN KEY (Microchip) REFERENCES Cane (Microchip);

ALTER TABLE Entrata ADD FOREIGN KEY (Operatore) REFERENCES Operatore (CF);

ALTER TABLE Uscita ADD FOREIGN KEY (n_reg_entrata) REFERENCES Entrata (n_progressivo);

ALTER TABLE Usa_Veicolo ADD FOREIGN KEY (Targa_veicolo) REFERENCES Veicolo (Targa);

ALTER TABLE Usa_Veicolo ADD FOREIGN KEY (Operatore) REFERENCES Operatore (CF);

ALTER TABLE Lavora_su ADD FOREIGN KEY (Operatore) REFERENCES Operatore (CF);

ALTER TABLE Lavora_su ADD FOREIGN KEY (Missione) REFERENCES Missione (N_Missione);
