﻿prompt PL/SQL Developer import file
prompt Created on domenica 10 giugno 2012 by Administrator
set feedback off
set define off
prompt Creating ARMADIETTO...
create table ARMADIETTO
(
  numero       NUMBER(5) not null,
  combinazione CHAR(6),
  dimensione   NUMBER(2)
)
;
alter table ARMADIETTO
  add primary key (NUMERO);

prompt Creating ASL...
create table ASL
(
  n_asl        CHAR(20) not null,
  responsabile CHAR(20),
  cap          NUMBER(6),
  civico       NUMBER(4),
  via          CHAR(50),
  citta        CHAR(50)
)
;
alter table ASL
  add primary key (N_ASL);

prompt Creating CUCCIA...
create table CUCCIA
(
  cod_cuccia     CHAR(20) not null,
  dimensione_mq2 NUMBER(10)
)
;
alter table CUCCIA
  add primary key (COD_CUCCIA);

prompt Creating PADRONE...
create table PADRONE
(
  codfisc CHAR(16) not null,
  cognome CHAR(20) not null,
  nome    CHAR(20) not null,
  citta   CHAR(20),
  tel     CHAR(10),
  cap     NUMBER(6),
  civico  NUMBER(6),
  via     CHAR(50)
)
;
alter table PADRONE
  add primary key (CODFISC);

prompt Creating RAZZE...
create table RAZZE
(
  cod_razza CHAR(20) not null,
  nome      CHAR(20)
)
;
alter table RAZZE
  add primary key (COD_RAZZA);

prompt Creating CANE...
create table CANE
(
  microchip    CHAR(20) not null,
  data_nascita DATE,
  sesso        CHAR(1),
  padrone      CHAR(16),
  cuccia       CHAR(20),
  razza        CHAR(20)
)
;
alter table CANE
  add primary key (MICROCHIP);
alter table CANE
  add foreign key (PADRONE)
  references PADRONE (CODFISC);
alter table CANE
  add foreign key (CUCCIA)
  references CUCCIA (COD_CUCCIA);
alter table CANE
  add foreign key (RAZZA)
  references RAZZE (COD_RAZZA);

prompt Creating COMUNE...
create table COMUNE
(
  codcomune CHAR(20) not null,
  nome      CHAR(20),
  sindaco   CHAR(20),
  asl       CHAR(20)
)
;
alter table COMUNE
  add primary key (CODCOMUNE);
alter table COMUNE
  add foreign key (ASL)
  references ASL (N_ASL);

prompt Creating DISTRETTO...
create table DISTRETTO
(
  numero NUMBER(5) not null,
  nome   CHAR(20),
  civico NUMBER(4),
  cap    NUMBER(6),
  via    CHAR(50),
  citta  CHAR(50)
)
;
alter table DISTRETTO
  add primary key (NUMERO);

prompt Creating OPERATORE...
create table OPERATORE
(
  cf         CHAR(16) not null,
  cognome    CHAR(20) not null,
  nome       CHAR(20) not null,
  indirizzo  CHAR(50),
  citta      CHAR(20),
  tel        CHAR(10),
  cell       CHAR(20),
  salario    NUMBER(10) not null,
  distretto  NUMBER(5),
  armadietto NUMBER(5)
)
;
alter table OPERATORE
  add primary key (CF);
alter table OPERATORE
  add foreign key (DISTRETTO)
  references DISTRETTO (NUMERO);
alter table OPERATORE
  add foreign key (ARMADIETTO)
  references ARMADIETTO (NUMERO);

prompt Creating ENTRATA...
create table ENTRATA
(
  n_progressivo CHAR(20) not null,
  datae         DATE,
  modoe         CHAR(20),
  comune        CHAR(20),
  microchip     CHAR(20),
  operatore     CHAR(16)
)
;
alter table ENTRATA
  add primary key (N_PROGRESSIVO);
alter table ENTRATA
  add foreign key (COMUNE)
  references COMUNE (CODCOMUNE);
alter table ENTRATA
  add foreign key (MICROCHIP)
  references CANE (MICROCHIP);
alter table ENTRATA
  add foreign key (OPERATORE)
  references OPERATORE (CF);

prompt Creating FOTO...
create table FOTO
(
  url       CHAR(20) not null,
  data      DATE,
  microchip CHAR(20)
)
;
alter table FOTO
  add primary key (URL);
alter table FOTO
  add foreign key (MICROCHIP)
  references CANE (MICROCHIP);

prompt Creating MISSIONE...
create table MISSIONE
(
  nome_missione CHAR(20) not null,
  n_missione    NUMBER(3) not null,
  loc_missione  CHAR(50),
  distretto     NUMBER(5)
)
;
alter table MISSIONE
  add primary key (N_MISSIONE);
alter table MISSIONE
  add foreign key (DISTRETTO)
  references DISTRETTO (NUMERO);

prompt Creating LAVORA_SU...
create table LAVORA_SU
(
  operatore CHAR(16) not null,
  missione  NUMBER(3) not null,
  ore       NUMBER(4)
)
;
alter table LAVORA_SU
  add primary key (OPERATORE, MISSIONE);
alter table LAVORA_SU
  add foreign key (OPERATORE)
  references OPERATORE (CF);
alter table LAVORA_SU
  add foreign key (MISSIONE)
  references MISSIONE (N_MISSIONE);

prompt Creating VEICOLO...
create table VEICOLO
(
  targa CHAR(10) not null,
  posti NUMBER(2)
)
;
alter table VEICOLO
  add primary key (TARGA);

prompt Creating USA_VEICOLO...
create table USA_VEICOLO
(
  targa_veicolo CHAR(10) not null,
  operatore     CHAR(16) not null
)
;
alter table USA_VEICOLO
  add primary key (TARGA_VEICOLO, OPERATORE);
alter table USA_VEICOLO
  add foreign key (TARGA_VEICOLO)
  references VEICOLO (TARGA);
alter table USA_VEICOLO
  add foreign key (OPERATORE)
  references OPERATORE (CF);

prompt Creating USCITA...
create table USCITA
(
  n_reg_uscita  CHAR(20) not null,
  n_reg_entrata CHAR(20),
  datau         DATE,
  modou         CHAR(20),
  note          CHAR(20)
)
;
alter table USCITA
  add primary key (N_REG_USCITA);
alter table USCITA
  add foreign key (N_REG_ENTRATA)
  references ENTRATA (N_PROGRESSIVO);

prompt Creating VISITA...
create table VISITA
(
  microchip CHAR(20) not null,
  data      DATE not null,
  diagnosi  CHAR(20),
  terapia   CHAR(20)
)
;
alter table VISITA
  add primary key (MICROCHIP, DATA);
alter table VISITA
  add foreign key (MICROCHIP)
  references CANE (MICROCHIP);

prompt Loading ARMADIETTO...
insert into ARMADIETTO (numero, combinazione, dimensione)
values (169, '764KTJ', 2);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (866, '983XVH', 3);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (345, '174HBI', 2);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (612, '938QAS', 3);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (796, '862IER', 2);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (963, '425VOD', 3);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (283, '783PWU', 2);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (868, '394RCA', 3);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (723, '892WTG', 2);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (367, '159FQM', 3);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (312, '448KDY', 1);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (996, '937NLZ', 3);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (931, '383NOP', 2);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (888, '114ATX', 1);
insert into ARMADIETTO (numero, combinazione, dimensione)
values (291, '164TTP', 2);
commit;
prompt 15 records loaded
prompt Loading ASL...
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('973                 ', 'Sinead Posener      ', 29216, 21, 'Lodi Road                                         ', 'Steyr                                             ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('392                 ', 'Grace Weaving       ', 87658, 57, 'Warrington Blvd                                   ', 'Mechelen                                          ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('161                 ', 'Albert Gandolfini   ', 91494, 25, 'Tia Ave                                           ', 'Amherst                                           ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('413                 ', 'Marina LaBelle      ', 93366, 75, 'Fred Street                                       ', 'Farnham                                           ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('918                 ', 'Patti Parker        ', 77365, 78, 'Tripplehorn Road                                  ', 'Anyang-si                                         ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('774                 ', 'Jodie Morrison      ', 45318, 59, 'Nanaimo Ave                                       ', 'Neuquen                                           ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('672                 ', 'Glenn Flanagan      ', 15855, 13, 'Pantoliano Drive                                  ', 'New Hyde Park                                     ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('459                 ', 'Sinead Stallone     ', 68689, 51, 'Franks                                            ', 'Roma                                              ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('164                 ', 'Lin Simpson         ', 45856, 54, 'Columbus Blvd                                     ', 'Knutsford                                         ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('797                 ', 'Marty Assante       ', 64329, 58, 'Teri Drive                                        ', 'Pirapora bom Jesus                                ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('322                 ', 'Bill Salonga        ', 73257, 42, 'Chandler Ave                                      ', 'Nagasaki                                          ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('269                 ', 'Helen Belle         ', 42496, 45, 'Gates Street                                      ', 'Sparrows Point                                    ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('928                 ', 'Robby Karyo         ', 91472, 65, 'Gary Road                                         ', 'Echirolles                                        ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('123                 ', 'Rhona Begley        ', 31183, 11, 'Zappacosta Road                                   ', 'Belp                                              ');
insert into ASL (n_asl, responsabile, cap, civico, via, citta)
values ('658                 ', 'Rory Pesci          ', 63857, 82, 'Holland Drive                                     ', 'Sutton                                            ');
commit;
prompt 15 records loaded
prompt Loading CUCCIA...
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('79                  ', 4);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('84                  ', 3);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('88                  ', 5);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('95                  ', 4);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('58                  ', 3);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('49                  ', 4);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('78                  ', 5);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('42                  ', 3);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('37                  ', 3);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('98                  ', 4);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('15                  ', 5);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('51                  ', 5);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('23                  ', 4);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('27                  ', 3);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('97                  ', 3);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('65                  ', 3);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('34                  ', 4);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('12                  ', 5);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('17                  ', 4);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('63                  ', 5);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('74                  ', 4);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('29                  ', 3);
insert into CUCCIA (cod_cuccia, dimensione_mq2)
values ('45                  ', 4);
commit;
prompt 23 records loaded
prompt Loading PADRONE...
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('NKVZ8457LMEW3878', 'Reed                ', 'Burt                ', 'Vantaa              ', '136929699 ', 71178, 66, 'Benet Ave                                         ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('KZGZ7332XAIG3941', 'Masur               ', 'Bill                ', 'Shreveport          ', '931624539 ', 15973, 29, 'Tbilisi Street                                    ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('LEUS5687KVMH7219', 'Jovovich            ', 'Lydia               ', 'Paço de Arcos      ', '996554478 ', 47613, 26, 'Kershaw Road                                      ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('NLIF2488QASD3387', 'Lopez               ', 'Embeth              ', 'Essen               ', '441628537 ', 24722, 88, 'Bancroft Blvd                                     ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('HBAH6433QFXR5811', 'Berkoff             ', 'Raul                ', 'Orange              ', '869295228 ', 44796, 71, 'Kershaw Street                                    ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('MCUO9635ISRQ8658', 'Makowicz            ', 'Kevin               ', 'West Chester        ', '882159251 ', 67523, 95, 'Rob Street                                        ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('OAJO1347ZQZA1337', 'Deejay              ', 'Rosanne             ', 'Charlotte           ', '666924696 ', 73436, 38, 'Calgary Street                                    ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('QSOI5681ZMAE5263', 'Himmelman           ', 'Fairuza             ', 'Morioka             ', '735219857 ', 58724, 83, 'Condition Road                                    ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('VKWX2497CFXV5869', 'Kinski              ', 'Chuck               ', 'Stellenbosch        ', '898516769 ', 41237, 23, 'Gibbons Drive                                     ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('QUYA1717NBJZ4286', 'Monk                ', 'Arturo              ', 'Baltimore           ', '249174981 ', 85812, 26, 'Colorado Springs                                  ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('FULL4696ZAKV9963', 'Beck                ', 'Keanu               ', 'Ingelheim           ', '774163498 ', 74628, 85, 'Bremen Blvd                                       ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('NSQL7659CRUV5769', 'Molina              ', 'Darren              ', 'Monument            ', '524859868 ', 59914, 37, 'Fiorentino                                        ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('VGPP7565RKGN9999', 'Faithfull           ', 'Manu                ', 'Paris               ', '411861469 ', 59221, 69, 'Leoni Street                                      ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('CEOM3139LNIF7658', 'Sedgwick            ', 'Nanci               ', 'Shelton             ', '958279875 ', 11693, 39, 'Crouse Drive                                      ');
insert into PADRONE (codfisc, cognome, nome, citta, tel, cap, civico, via)
values ('PEXI7519OQOA2323', 'Lattimore           ', 'Ming-Na             ', 'Santiago            ', '421851331 ', 85421, 39, 'Akron Drive                                       ');
commit;
prompt 15 records loaded
prompt Loading RAZZE...
insert into RAZZE (cod_razza, nome)
values ('MZT-286             ', 'Bracco Italiano     ');
insert into RAZZE (cod_razza, nome)
values ('GXP-133             ', 'Maltese             ');
insert into RAZZE (cod_razza, nome)
values ('RCD-994             ', 'Bolognese           ');
insert into RAZZE (cod_razza, nome)
values ('TQK-573             ', 'Segugio di Cravin   ');
insert into RAZZE (cod_razza, nome)
values ('RQG-623             ', 'Bracco Italiano     ');
insert into RAZZE (cod_razza, nome)
values ('HZQ-538             ', 'Bolognese           ');
insert into RAZZE (cod_razza, nome)
values ('GGW-257             ', 'Levriero Meridionale');
insert into RAZZE (cod_razza, nome)
values ('NDR-986             ', 'Maltese             ');
insert into RAZZE (cod_razza, nome)
values ('RDG-882             ', 'Bolognese           ');
insert into RAZZE (cod_razza, nome)
values ('MDM-341             ', 'Segugio di Cravin   ');
insert into RAZZE (cod_razza, nome)
values ('CMD-415             ', 'Bolognese           ');
insert into RAZZE (cod_razza, nome)
values ('BNR-715             ', 'Maltese             ');
insert into RAZZE (cod_razza, nome)
values ('OSF-726             ', 'Bracco Italiano     ');
insert into RAZZE (cod_razza, nome)
values ('VSD-582             ', 'Mastino Napoletano  ');
insert into RAZZE (cod_razza, nome)
values ('MCI-219             ', 'Bolognese           ');
commit;
prompt 15 records loaded
prompt Loading CANE...
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('897VPW              ', to_date('05-06-2012', 'dd-mm-yyyy'), 'F', 'CEOM3139LNIF7658', '12                  ', 'CMD-415             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('375RXO              ', to_date('18-06-2012', 'dd-mm-yyyy'), 'M', 'CEOM3139LNIF7658', '23                  ', 'GXP-133             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('561FDS              ', to_date('28-06-2012', 'dd-mm-yyyy'), 'M', 'HBAH6433QFXR5811', '29                  ', 'MCI-219             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('398IXJ              ', to_date('04-06-2012', 'dd-mm-yyyy'), 'F', 'NKVZ8457LMEW3878', '37                  ', 'NDR-986             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('239FJG              ', to_date('14-06-2012', 'dd-mm-yyyy'), 'M', 'LEUS5687KVMH7219', '23                  ', 'NDR-986             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('325DCJ              ', to_date('18-06-2012', 'dd-mm-yyyy'), 'M', 'NLIF2488QASD3387', '34                  ', 'GGW-257             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('131WES              ', to_date('11-06-2012', 'dd-mm-yyyy'), 'F', 'NSQL7659CRUV5769', '42                  ', 'TQK-573             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('886KTM              ', to_date('21-06-2012', 'dd-mm-yyyy'), 'M', 'PEXI7519OQOA2323', '42                  ', 'TQK-573             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('748FKP              ', to_date('11-06-2012', 'dd-mm-yyyy'), 'M', 'KZGZ7332XAIG3941', '37                  ', 'RDG-882             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('737FGN              ', to_date('23-06-2012', 'dd-mm-yyyy'), 'F', 'NLIF2488QASD3387', '95                  ', 'OSF-726             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('977FDB              ', to_date('06-06-2012', 'dd-mm-yyyy'), 'M', 'QSOI5681ZMAE5263', '42                  ', 'RQG-623             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('116RMB              ', to_date('21-03-2012', 'dd-mm-yyyy'), 'F', 'QUYA1717NBJZ4286', '27                  ', 'RQG-623             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('236QSE              ', to_date('18-06-2012', 'dd-mm-yyyy'), 'M', 'QUYA1717NBJZ4286', '42                  ', 'TQK-573             ');
insert into CANE (microchip, data_nascita, sesso, padrone, cuccia, razza)
values ('928KFC              ', to_date('25-06-2012', 'dd-mm-yyyy'), 'F', 'NKVZ8457LMEW3878', '27                  ', 'GXP-133             ');
commit;
prompt 14 records loaded
prompt Loading COMUNE...
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('691                 ', 'Paal Beringen       ', 'Ritchie Cherry      ', '123                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('471                 ', 'Ipswich             ', 'Teri Arquette       ', '161                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('865                 ', 'Reisterstown        ', 'Gerald Affleck      ', '161                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('641                 ', 'Kingston            ', 'Thelma Paige        ', '658                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('294                 ', 'Carmichael          ', 'Cornell Gatlin      ', '392                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('779                 ', 'Sapulpa             ', 'Wayne Stone         ', '774                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('867                 ', 'Karachi             ', 'Patrick Bell        ', '973                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('573                 ', 'Luzern              ', 'Austin Tanon        ', '322                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('587                 ', 'League city         ', 'Frankie Day-Lewis   ', '459                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('679                 ', 'Ribeirao preto      ', 'Hilary Gilliam      ', '413                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('723                 ', 'Kejae City          ', 'Buddy Sossamon      ', '658                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('713                 ', 'Aomori              ', 'Laura Rooker        ', '797                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('117                 ', 'Trumbull            ', 'Mike Piven          ', '322                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('398                 ', 'Horb                ', 'Cameron Bates       ', '413                 ');
insert into COMUNE (codcomune, nome, sindaco, asl)
values ('358                 ', 'Warrenton           ', 'Collective McNeice  ', '658                 ');
commit;
prompt 15 records loaded
prompt Loading DISTRETTO...
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (869, 'Software            ', 89, 95422, 'Tal Street                                        ', 'Holts Summit                                      ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (989, 'Amministrazione     ', 41, 15584, 'Mendoza Blvd                                      ', 'Pulheim-brauweiler                                ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (951, 'Quartier Generale   ', 57, 11623, 'Larnelle Drive                                    ', 'Happy Valley                                      ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (692, 'Quartier Generale   ', 53, 55554, 'Oates                                             ', 'Slough                                            ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (167, 'Amministrazione     ', 53, 75694, 'Hewitt Drive                                      ', 'Adelaide                                          ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (814, 'Software            ', 19, 46217, 'Lewin Road                                        ', 'Redmond                                           ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (667, 'Vendite             ', 16, 43247, 'Badalucco Road                                    ', 'Oldham                                            ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (795, 'Quartier Generale   ', 13, 94187, 'Wetzlar Road                                      ', 'Bellevue                                          ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (787, 'Hardware            ', 49, 23492, 'Cantrell Drive                                    ', 'Issaquah                                          ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (754, 'Ricerca             ', 16, 64559, 'Hermitage Ave                                     ', 'Shreveport                                        ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (632, 'Vendite             ', 58, 31341, 'Yep Street                                        ', 'Durban                                            ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (651, 'Hardware            ', 91, 58458, 'Nigel Blvd                                        ', 'Perth                                             ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (756, 'Vendite             ', 66, 18195, 'Lachey Road                                       ', 'Genève                                           ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (518, 'Hardware            ', 93, 41883, 'Holland Street                                    ', 'Issaquah                                          ');
insert into DISTRETTO (numero, nome, civico, cap, via, citta)
values (594, 'Ricerca             ', 46, 77321, 'Dafoe Drive                                       ', 'Bolzano                                           ');
commit;
prompt 15 records loaded
prompt Loading OPERATORE...
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('KPKN8175SEES5789', 'Blades              ', 'Lara                ', '39 Grand-mere Drive                               ', 'Aomori              ', '166451268 ', null, 1052, 518, 283);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('MJBN7963EQPG1372', 'Wright              ', 'Madeline            ', '583 O''Neill Road                                  ', 'Hearst              ', '316633331 ', null, 2666, 518, 169);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('URVO7139LSVE1625', 'O''Donnell           ', 'Walter              ', '84 Dupree Drive                                   ', 'Grapevine           ', '144399917 ', null, 1952, 692, 367);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('LDEG1279WWUV2767', 'Pride               ', 'Joanna              ', '63 Spine Drive                                    ', 'Cerritos            ', '884121812 ', null, 1350, 756, 312);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('ELZJ9171VWEJ8389', 'Chung               ', 'Aidan               ', '57 Lily Drive                                     ', 'Athens              ', '618255163 ', null, 1322, 756, 367);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('WACP4354OFKF7842', 'Soda                ', 'Gavin               ', '76 Toledo Ave                                     ', 'Fornacette          ', '933383885 ', null, 2490, 951, 723);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('CFVO5774ZMAS3351', 'Badalucco           ', 'Jonathan            ', '948 Payne Ave                                     ', 'Dublin              ', '475877591 ', null, 2617, 989, 345);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('ICMB2816OCEY8482', 'Archer              ', 'Cliff               ', '687 Vannelli Drive                                ', 'University          ', '171923223 ', null, 1747, 756, 345);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('PHYQ1841MSWF7277', 'Orlando             ', 'Rhys                ', '47 Almond Street                                  ', 'Royersford          ', '583849919 ', null, 1448, 167, 612);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('AJNG4373TBBH6921', 'Barkin              ', 'Clive               ', '98 Malcolm Blvd                                   ', 'Snoqualmie          ', '498328537 ', null, 2163, 754, 888);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('VLMP3585ESOH9685', 'Connick             ', 'Juliet              ', '2 Shannon Road                                    ', 'New boston          ', '776342252 ', null, 2879, 651, 796);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('FULC4971EGWD3639', 'Frost               ', 'Clive               ', '705 Lowell Drive                                  ', 'St. Petersburg      ', '637746116 ', null, 2515, 787, 931);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('FTAP2766QQON9628', 'Hubbard             ', 'Loretta             ', '64 Sophie Drive                                   ', 'Claymont            ', '357349573 ', null, 1263, 594, 723);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('ZUPC8742YRNU8887', 'Bailey              ', 'Ray                 ', '36 Idol Road                                      ', 'Hilversum           ', '711857596 ', null, 1240, 692, 612);
insert into OPERATORE (cf, cognome, nome, indirizzo, citta, tel, cell, salario, distretto, armadietto)
values ('EASL9498YBGI2222', 'Cattrall            ', 'Cherry              ', '95 Snow Road                                      ', 'Tokyo               ', '881979715 ', null, 1846, 667, 723);
commit;
prompt 15 records loaded
prompt Loading ENTRATA...
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('48                  ', to_date('04-06-2012', 'dd-mm-yyyy'), null, '573                 ', '116RMB              ', 'ICMB2816OCEY8482');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('68                  ', to_date('18-06-2012', 'dd-mm-yyyy'), null, '641                 ', '131WES              ', 'FTAP2766QQON9628');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('97                  ', to_date('19-06-2012', 'dd-mm-yyyy'), null, '587                 ', '236QSE              ', 'KPKN8175SEES5789');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('29                  ', to_date('03-06-2012', 'dd-mm-yyyy'), null, '587                 ', '325DCJ              ', 'ICMB2816OCEY8482');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('61                  ', to_date('21-06-2012', 'dd-mm-yyyy'), null, '641                 ', '375RXO              ', 'ICMB2816OCEY8482');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('79                  ', to_date('17-06-2012', 'dd-mm-yyyy'), null, '779                 ', '748FKP              ', 'URVO7139LSVE1625');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('89                  ', to_date('20-06-2012', 'dd-mm-yyyy'), null, '865                 ', '561FDS              ', 'VLMP3585ESOH9685');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('26                  ', to_date('20-06-2012', 'dd-mm-yyyy'), null, '691                 ', '897VPW              ', 'VLMP3585ESOH9685');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('18                  ', to_date('25-06-2012', 'dd-mm-yyyy'), null, '867                 ', '886KTM              ', 'FTAP2766QQON9628');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('37                  ', to_date('12-06-2012', 'dd-mm-yyyy'), null, '641                 ', '748FKP              ', 'ZUPC8742YRNU8887');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('96                  ', to_date('15-03-2013', 'dd-mm-yyyy'), null, '865                 ', '928KFC              ', 'FTAP2766QQON9628');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('87                  ', to_date('27-07-2012', 'dd-mm-yyyy'), null, '679                 ', '737FGN              ', 'LDEG1279WWUV2767');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('12                  ', to_date('18-04-2012', 'dd-mm-yyyy'), null, '867                 ', '737FGN              ', 'WACP4354OFKF7842');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('22                  ', to_date('09-05-2012', 'dd-mm-yyyy'), null, '471                 ', '977FDB              ', 'KPKN8175SEES5789');
insert into ENTRATA (n_progressivo, datae, modoe, comune, microchip, operatore)
values ('17                  ', to_date('23-05-2012', 'dd-mm-yyyy'), null, '471                 ', '928KFC              ', 'KPKN8175SEES5789');
commit;
prompt 15 records loaded
prompt Loading FOTO...
insert into FOTO (url, data, microchip)
values ('promethium.JPG      ', to_date('09-06-2012', 'dd-mm-yyyy'), '116RMB              ');
insert into FOTO (url, data, microchip)
values ('palladium.JPG       ', to_date('06-06-2012', 'dd-mm-yyyy'), '239FJG              ');
insert into FOTO (url, data, microchip)
values ('actinium.JPG        ', to_date('07-02-2012', 'dd-mm-yyyy'), '561FDS              ');
insert into FOTO (url, data, microchip)
values ('carbon.JPG          ', to_date('19-06-2012', 'dd-mm-yyyy'), '398IXJ              ');
insert into FOTO (url, data, microchip)
values ('rhenium.JPG         ', to_date('03-06-2012', 'dd-mm-yyyy'), '561FDS              ');
insert into FOTO (url, data, microchip)
values ('platinum.JPG        ', to_date('15-06-2012', 'dd-mm-yyyy'), '561FDS              ');
insert into FOTO (url, data, microchip)
values ('lawrencium.JPG      ', to_date('18-06-2012', 'dd-mm-yyyy'), '897VPW              ');
insert into FOTO (url, data, microchip)
values ('curium.JPG          ', to_date('28-06-2012', 'dd-mm-yyyy'), '398IXJ              ');
insert into FOTO (url, data, microchip)
values ('protactinium.JPG    ', to_date('11-06-2012', 'dd-mm-yyyy'), '561FDS              ');
insert into FOTO (url, data, microchip)
values ('cobalt.JPG          ', to_date('07-06-2012', 'dd-mm-yyyy'), '928KFC              ');
insert into FOTO (url, data, microchip)
values ('radium.JPG          ', to_date('10-06-2012', 'dd-mm-yyyy'), '561FDS              ');
insert into FOTO (url, data, microchip)
values ('rhodium.JPG         ', to_date('23-06-2012', 'dd-mm-yyyy'), '398IXJ              ');
insert into FOTO (url, data, microchip)
values ('francium.JPG        ', to_date('11-06-2012', 'dd-mm-yyyy'), '928KFC              ');
insert into FOTO (url, data, microchip)
values ('iodine.JPG          ', to_date('07-06-2012', 'dd-mm-yyyy'), '325DCJ              ');
insert into FOTO (url, data, microchip)
values ('tin.JPG             ', to_date('21-06-2012', 'dd-mm-yyyy'), '561FDS              ');
commit;
prompt 15 records loaded
prompt Loading MISSIONE...
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneB           ', 95, 'Duesseldorf                                       ', 518);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneY           ', 16, 'Hässleholm                                       ', 594);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneF           ', 99, 'Oosterhout                                        ', 787);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneT           ', 35, 'Warrington                                        ', 632);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneL           ', 68, 'Brasília                                         ', 869);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneL           ', 24, 'Stellenbosch                                      ', 667);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneW           ', 84, 'Suffern                                           ', 951);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneM           ', 22, 'Pomona                                            ', 667);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneZ           ', 23, 'Augst                                             ', 692);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneZ           ', 33, 'Birmingham                                        ', 692);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneS           ', 67, 'Solon                                             ', 989);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneJ           ', 56, 'Redding                                           ', 756);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneB           ', 41, 'Münster                                          ', 667);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneY           ', 65, 'Tallahassee                                       ', 869);
insert into MISSIONE (nome_missione, n_missione, loc_missione, distretto)
values ('MissioneB           ', 43, 'North Yorkshire                                   ', 667);
commit;
prompt 15 records loaded
prompt Loading LAVORA_SU...
insert into LAVORA_SU (operatore, missione, ore)
values ('AJNG4373TBBH6921', 16, 1);
insert into LAVORA_SU (operatore, missione, ore)
values ('AJNG4373TBBH6921', 22, 4);
insert into LAVORA_SU (operatore, missione, ore)
values ('CFVO5774ZMAS3351', 22, 6);
insert into LAVORA_SU (operatore, missione, ore)
values ('EASL9498YBGI2222', 24, 1);
insert into LAVORA_SU (operatore, missione, ore)
values ('ELZJ9171VWEJ8389', 23, 4);
insert into LAVORA_SU (operatore, missione, ore)
values ('FTAP2766QQON9628', 35, 6);
insert into LAVORA_SU (operatore, missione, ore)
values ('FULC4971EGWD3639', 24, 7);
insert into LAVORA_SU (operatore, missione, ore)
values ('ICMB2816OCEY8482', 33, 5);
insert into LAVORA_SU (operatore, missione, ore)
values ('ICMB2816OCEY8482', 41, 8);
insert into LAVORA_SU (operatore, missione, ore)
values ('KPKN8175SEES5789', 56, 5);
insert into LAVORA_SU (operatore, missione, ore)
values ('LDEG1279WWUV2767', 43, 2);
insert into LAVORA_SU (operatore, missione, ore)
values ('MJBN7963EQPG1372', 68, 8);
insert into LAVORA_SU (operatore, missione, ore)
values ('PHYQ1841MSWF7277', 84, 8);
insert into LAVORA_SU (operatore, missione, ore)
values ('URVO7139LSVE1625', 67, 4);
insert into LAVORA_SU (operatore, missione, ore)
values ('VLMP3585ESOH9685', 65, 1);
insert into LAVORA_SU (operatore, missione, ore)
values ('WACP4354OFKF7842', 99, 5);
insert into LAVORA_SU (operatore, missione, ore)
values ('ZUPC8742YRNU8887', 95, 6);
insert into LAVORA_SU (operatore, missione, ore)
values ('FULC4971EGWD3639', 16, 7);
commit;
prompt 18 records loaded
prompt Loading VEICOLO...
insert into VEICOLO (targa, posti)
values ('578ZPF548 ', 10);
insert into VEICOLO (targa, posti)
values ('845NKK375 ', 3);
insert into VEICOLO (targa, posti)
values ('629RXN496 ', 10);
insert into VEICOLO (targa, posti)
values ('812QKX996 ', 3);
insert into VEICOLO (targa, posti)
values ('391VWX376 ', 4);
insert into VEICOLO (targa, posti)
values ('915RMT896 ', 6);
insert into VEICOLO (targa, posti)
values ('888GBY988 ', 6);
insert into VEICOLO (targa, posti)
values ('921FOG782 ', 3);
insert into VEICOLO (targa, posti)
values ('865YIT471 ', 8);
insert into VEICOLO (targa, posti)
values ('458DJL875 ', 11);
insert into VEICOLO (targa, posti)
values ('241AQK442 ', 7);
insert into VEICOLO (targa, posti)
values ('652MXE765 ', 7);
insert into VEICOLO (targa, posti)
values ('933OFP988 ', 5);
insert into VEICOLO (targa, posti)
values ('647BPJ212 ', 6);
insert into VEICOLO (targa, posti)
values ('847YIO784 ', 6);
commit;
prompt 15 records loaded
prompt Loading USA_VEICOLO...
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('241AQK442 ', 'FTAP2766QQON9628');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('391VWX376 ', 'FULC4971EGWD3639');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('458DJL875 ', 'FTAP2766QQON9628');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('578ZPF548 ', 'KPKN8175SEES5789');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('578ZPF548 ', 'LDEG1279WWUV2767');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('629RXN496 ', 'FULC4971EGWD3639');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('629RXN496 ', 'LDEG1279WWUV2767');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('647BPJ212 ', 'FULC4971EGWD3639');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('647BPJ212 ', 'ZUPC8742YRNU8887');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('652MXE765 ', 'FULC4971EGWD3639');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('652MXE765 ', 'ICMB2816OCEY8482');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('652MXE765 ', 'ZUPC8742YRNU8887');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('812QKX996 ', 'KPKN8175SEES5789');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('847YIO784 ', 'FULC4971EGWD3639');
insert into USA_VEICOLO (targa_veicolo, operatore)
values ('915RMT896 ', 'WACP4354OFKF7842');
commit;
prompt 15 records loaded
prompt Loading USCITA...
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('936                 ', '22                  ', to_date('10-05-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('777                 ', '61                  ', to_date('04-04-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('986                 ', '12                  ', to_date('13-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('389                 ', '68                  ', to_date('26-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('683                 ', '48                  ', to_date('20-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('845                 ', '48                  ', to_date('07-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('159                 ', '48                  ', to_date('05-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('324                 ', '26                  ', to_date('06-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('226                 ', '26                  ', to_date('04-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('859                 ', '96                  ', to_date('04-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('944                 ', '96                  ', to_date('07-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('277                 ', '89                  ', to_date('05-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('334                 ', '97                  ', to_date('04-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('541                 ', '96                  ', to_date('03-06-2012', 'dd-mm-yyyy'), null, null);
insert into USCITA (n_reg_uscita, n_reg_entrata, datau, modou, note)
values ('341                 ', '22                  ', to_date('12-04-2012', 'dd-mm-yyyy'), null, null);
commit;
prompt 15 records loaded
prompt Loading VISITA...
insert into VISITA (microchip, data, diagnosi, terapia)
values ('236QSE              ', to_date('10-06-2012', 'dd-mm-yyyy'), 'omnis minim in.     ', 'reprehenderit.      ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('398IXJ              ', to_date('11-06-2012', 'dd-mm-yyyy'), 'dolor optio.        ', 'quo id.             ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('398IXJ              ', to_date('27-06-2012', 'dd-mm-yyyy'), 'maiores excepteur.  ', 'et est.             ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('737FGN              ', to_date('21-06-2012', 'dd-mm-yyyy'), 'dignissimos eu.     ', 'et minus.           ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('398IXJ              ', to_date('12-06-2012', 'dd-mm-yyyy'), 'nulla culpa in.     ', 'quas at.            ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('375RXO              ', to_date('26-06-2012', 'dd-mm-yyyy'), 'ad incididunt aut.  ', 'nihil et.           ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('928KFC              ', to_date('20-06-2012', 'dd-mm-yyyy'), 'quas at.            ', 'quas at.            ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('977FDB              ', to_date('20-05-2012', 'dd-mm-yyyy'), 'ex elit cum.        ', 'dolor et.           ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('239FJG              ', to_date('12-04-2012', 'dd-mm-yyyy'), 'ut et ea id.        ', 'in.                 ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('886KTM              ', to_date('17-04-2012', 'dd-mm-yyyy'), 'et.                 ', 'quas at.            ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('375RXO              ', to_date('06-06-2012', 'dd-mm-yyyy'), 'quo aut.            ', 'maiores.            ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('748FKP              ', to_date('06-06-2012', 'dd-mm-yyyy'), 'nihil itaque.       ', 'rerum sapiente quis.');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('897VPW              ', to_date('07-06-2012', 'dd-mm-yyyy'), 'sunt.               ', 'commodo aut.        ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('737FGN              ', to_date('14-06-2012', 'dd-mm-yyyy'), 'et.                 ', 'esse.               ');
insert into VISITA (microchip, data, diagnosi, terapia)
values ('325DCJ              ', to_date('19-04-2012', 'dd-mm-yyyy'), 'sint laborum.       ', 'maiores non.        ');
commit;
prompt 15 records loaded
set feedback on
set define on
prompt Done.
